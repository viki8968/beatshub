$(document).ready(function() {
    $('.fade-in').css({ opacity: 0, display: 'flex' }).animate({
        opacity: 1
    }, 2000);
    $(window).scroll(function() {
        var scrollPosition = 200;
        var currentScrollPosition = $(window).scrollTop();
        if (scrollPosition > currentScrollPosition) {
            var value = currentScrollPosition / scrollPosition;
            var color = "rgba(256, 256, 256, " + value + ")";
            console.log(value, color);
            $(".navbar-light").css("background-color", color);
        } else {
            var color = "rgba(256, 256, 256, 1)";
            $(".navbar-light").css("background-color", color);
        }
    })
});